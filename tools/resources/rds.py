"""RDS Cluster, Instance(s) and Security Groups"""


from typing import List

from troposphere import (
    GetAtt,
    Output,
    Ref,
    Tags,
    Template,
)
from troposphere.ec2 import (
    SecurityGroup,
    SecurityGroupIngress,
)
from troposphere.rds import (
    DBCluster,
    DBSubnetGroup,
    DBInstance
)

from tools import Config

class Rds:
    """An RDS cluster with optional at-rest encryption and primary and
    replica instances"""

    invalid_encryption_sizes = ('micro', 'small', 'medium')

    def __init__(self,
                 t: Template,
                 subnets: List[Ref],
                 source_security_groups: List[Ref],
                 size: str,
                 name: str,
                 engine: str,
                 username: Ref,
                 password: Ref,
                 vpc: Ref,
                 encrypted: bool = False,
                 replica: bool = False):

        self.config = Config()

        self.cluster_sg = t.add_resource(SecurityGroup(
            'ClusterSecurityGroup',
            GroupDescription=f'{name} cluster security group',
            VpcId=vpc,
        ))

        for i, val in enumerate(source_security_groups):
            t.add_resource(SecurityGroupIngress(
                f'FromAppSg{i}',
                GroupId=Ref(self.cluster_sg),
                SourceSecurityGroupId=val,
                FromPort='3306',
                IpProtocol='tcp',
                ToPort='3306',
            ))

        self.subnet_group = t.add_resource(DBSubnetGroup(
            'DBSubnetGroup',
            DBSubnetGroupDescription=f'{name} subnets',
            SubnetIds=subnets,
        ))

        """TODO: implement this before we get to production
        if encrypted and not [i in size for i in invalid_encryption_sizes]:
            self.kms_key = t.add_resource(Key(
                'DbKmsKey',
                Description=f'Encryption key for {name}',
                EnableKeyRotation=True,
                KeyPolicy='', # TODO: This should be something?
                Tags=Tags(Env=self.config.env)
            ))
        """

        self.cluster = t.add_resource(DBCluster(
            'DbCluster',
            BackupRetentionPeriod=7,
            DatabaseName=f'{self.config.env.title()}{name}',
            DBSubnetGroupName=Ref(self.subnet_group),
            Engine=engine,
            MasterUsername=username,
            MasterUserPassword=password,
            PreferredMaintenanceWindow='sun:08:00-sun:09:00',
            VpcSecurityGroupIds=[Ref(self.cluster_sg)],
        ))

        self.master = t.add_resource(DBInstance(
            'DbMaster',
            DBClusterIdentifier=Ref(self.cluster),
            DBInstanceClass=size,
            Engine=engine,
            PubliclyAccessible=False,
            Tags=Tags(
                Env=self.config.env,
            ),
        ))

        t.add_output(Output(
            f'{name.title()}PrimaryDb',
            Value=GetAtt(self.master, 'Endpoint.Address'),
            Description=f'{name.title()} primary database endpoint',
        ))

        if replica:
            self.replica = t.add_resource(DBInstance(
                'DbReplica',
                DBClusterIdentifier=Ref(self.cluster),
                DBInstanceClass=size,
                Engine=engine,
                PubliclyAccessible=False,
                SourceDBInstanceIdentifier=Ref(self.master),
                Tags=Tags(
                    Env=self.config.env,
                ),
            ))

            t.add_output(Output(
                f'{name.title()}ReplicaDb',
                Value=GetAtt(self.replica, 'Endpoint.Address'),
                Description=f'{name.title()} replica database endpoint',
            ))
