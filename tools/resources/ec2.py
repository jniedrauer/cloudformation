"""Wrappers for creating EC2 instances"""

from troposphere import (
    Base64,
    Ref,
    Sub,
    Tags,
    Template,
)
from troposphere.ec2 import (
    BlockDeviceMapping,
    EBSBlockDevice,
    EIPAssociation,
    Instance,
)
from troposphere.policies import (
    CreationPolicy,
    ResourceSignal,
)
from troposphere.cloudformation import (
    Init,
    InitConfig,
    InitConfigSets,
    Metadata,
)

from tools import Config
from tools.render import render_template


def get_device_mapping(idx):
    """Given an integer, return a unix device"""
    return chr(98+idx) # First device name will be /dev/sdb or /dev/sdc depending on whether or not
                       # the root volume was overriden


class Ec2:
    """An EC2 instance with metadata"""

    def __init__(self, t: Template,
                 name: str,
                 subnet: Ref,
                 security_groups: list,
                 size: str,
                 tags: dict,
                 profile: Ref = None,
                 volumes: list = None,
                 ami: str = None,
                 managed: bool = True,
                 eip: str = None,
                ):

        self.config = Config()

        self.name = name

        tags['Name'] = '.'.join([
            self.name,
            self.config.hosted_zones.internal.name,
        ]).lower()

        self.userdata = render_template('UserData.sh', resource=self.name)
        if not volumes:
            volumes = []
        for idx, volume in enumerate(volumes):
            if volume['mountpoint'] == '/':
                volume['device_id'] = 'a1'
            else:
                volume['device_id'] = get_device_mapping(idx)

            volume['volume'] = EBSBlockDevice(
                f'{name}Volume{idx}',
                VolumeSize=volume['size'],
                VolumeType='gp2',
                DeleteOnTermination=not volume.get('preserve'),
            )

            volume['mapping'] = BlockDeviceMapping(
                DeviceName=f'/dev/sd{volume["device_id"]}',
                Ebs=volume['volume'],
            )

        options = {}
        if profile:
            options.update(dict(
                IamInstanceProfile=profile,
            ))
        if volumes:
            options.update(dict(
                BlockDeviceMappings=[i['mapping'] for i in volumes],
            ))
        if managed:
            options.update(dict(
                UserData=Base64(Sub(self.userdata)),
                Metadata=Metadata(Init(
                    InitConfigSets(
                        default=['default'],
                    ),
                    default=InitConfig(
                        files={
                            '/tmp/CfnInitDefault.sh': {
                                'content': render_template(
                                    'CfnInitDefault.sh',
                                    hostname=tags['Name'],
                                    volumes=volumes,
                                ),
                                'mode': '000755',
                                'owner': 'root',
                                'group': 'root',
                            },
                        },
                        commands={
                            '1_default': {
                                'command': '/tmp/CfnInitDefault.sh',
                            },
                        },
                    ),
                )),
                CreationPolicy=CreationPolicy(ResourceSignal=ResourceSignal(
                    Timeout='PT5M',
                    Count=1,
                )),
            ))

        self.instance = t.add_resource(Instance(
            self.name,
            ImageId=ami or self.config.region.AMIs.centos7.v1801_01,
            InstanceType=size,
            KeyName=self.config.ec2_keypair,
            SecurityGroupIds=security_groups,
            SubnetId=subnet,
            Tags=Tags(**tags, **{'Env': self.config.env}),
            **options,
        ))

        if eip:
            t.add_resource(EIPAssociation(
                f'{self.name}EipAssociation',
                AllocationId=eip,
                InstanceId=Ref(self.instance),
            ))
