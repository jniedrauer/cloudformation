"""ALB helpers"""


from typing import List

from troposphere.certificatemanager import Certificate
from troposphere import (
    Ref,
    Tags,
    Template,
)
from troposphere.elasticloadbalancingv2 import (
    Action,
    Certificate as AlbCertificate,
    Listener,
    LoadBalancer,
    Matcher,
    TargetDescription,
    TargetGroup,
    TargetGroupAttribute,
)

from tools import Config


class Alb:
    """Create an ALB with web listeners and a TLS certificate"""

    def __init__(self, t: Template,
                 name: str,
                 subnets: List[Ref],
                 backend_port: int,
                 security_group: Ref,
                 healthcheck_path: str,
                 SANs: List[str],
                 vpc: Ref,
                 targets: List[Ref] = None,
                ):

        self.config = Config()

        if not targets:
            targets = []

        self.alb = t.add_resource(LoadBalancer(
            name,
            Scheme='internet-facing',
            SecurityGroups=[security_group],
            Subnets=subnets,
            Tags=Tags(
                Name=name,
                Env=self.config.env,
            ),
        ))

        self.target = t.add_resource(TargetGroup(
            f'{name}Target',
            HealthCheckIntervalSeconds=30,
            HealthCheckPath=healthcheck_path,
            HealthCheckProtocol='HTTP',
            HealthCheckTimeoutSeconds=10,
            HealthyThresholdCount=2,
            UnhealthyThresholdCount=2,
            Matcher=Matcher(
                HttpCode='200',
            ),
            Port=backend_port,
            Protocol='HTTP',
            Targets=[
                TargetDescription(
                    Id=i,
                )
                for i in targets
            ],
            VpcId=vpc,
            TargetGroupAttributes=[TargetGroupAttribute(
                Key='deregistration_delay.timeout_seconds',
                Value='30',
            )],
            Tags=Tags(
                Name=name,
                Env=self.config.env,
            ),
        ))

        self.cert = t.add_resource(Certificate(
            f'{name}Certificate',
            DomainName=SANs[0],
            SubjectAlternativeNames=SANs
        ))

        t.add_resource(Listener(
            f'{name}HttpListener',
            Port='80',
            Protocol='HTTP',
            LoadBalancerArn=Ref(self.alb),
            DefaultActions=[
                Action(
                    Type='forward',
                    TargetGroupArn=Ref(self.target)
                )
            ]
        ))

        t.add_resource(Listener(
            f'{name}HttpsListener',
            Port='443',
            Protocol='HTTPS',
            LoadBalancerArn=Ref(self.alb),
            Certificates=[
                AlbCertificate(
                    CertificateArn=Ref(self.cert),
                ),
            ],
            DefaultActions=[
                Action(
                    Type='forward',
                    TargetGroupArn=Ref(self.target),
                ),
            ],
        ))
