# pylint: disable=wrong-import-position

def split_list(lst):
    """Split a list in half"""
    half = int(len(lst) / 2)
    return lst[:half], lst[half:]

from .alb import Alb
from .ec2 import Ec2
from .ecs import Ecs
from .rds import Rds
from .subnet import Subnet
