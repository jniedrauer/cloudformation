"""Wrappers for creating ECS clusters"""


import textwrap
from typing import Dict, List

from troposphere import (
    Base64,
    Parameter,
    Sub,
    Ref,
    Template,
)
from troposphere.autoscaling import (
    Metadata,
    AutoScalingGroup,
    Tag as AsgTag,
    LaunchConfiguration,
)
from troposphere.ecs import (
    Cluster,
)
from troposphere.iam import InstanceProfile
from troposphere.policies import (
    AutoScalingCreationPolicy,
    AutoScalingReplacingUpdate,
    AutoScalingRollingUpdate,
    CreationPolicy,
    ResourceSignal,
    UpdatePolicy,
)
from troposphere.cloudformation import (
    Init,
    InitConfig,
    InitFiles,
    InitFile,
    InitServices,
    InitService,
)

from tools import Config
from tools.render import render_template
from . import get_device_mapping


VOLUME_OFFSET = 2 # ECS AMI has two volumes by default


class Ecs:
    """An ECS cluster with an EC2 autoscaling group"""

    def __init__(self, t: Template,
                 tags: Dict[str, str],
                 subnets: list,
                 security_groups: list,
                 size: Dict[str, int],
                 instance_type: str,
                 role: Ref,
                 ec2_volumes: List[dict] = None,
                 commands: List[str] = None):

        if not ec2_volumes:
            ec2_volumes = []

        if not commands:
            commands = []

        config = Config()

        self.userdata = render_template('EcsUserData.sh',
                                        metadata='LaunchConfiguration',
                                        resource='AutoscalingGroup')

        self.cluster = t.add_resource(Cluster(
            'EcsCluster',
        ))

        self.instance_profile = t.add_resource(InstanceProfile(
            'EcsInstanceProfile',
            Roles=[role]
        ))

        ecs_ami = t.add_parameter(Parameter(
            'EcsAmiId',
            Description='Latest ECS optimized AMI',
            Type='AWS::SSM::Parameter::Value<AWS::EC2::Image::Id>',
            Default=config.region.AMIs.ecs.parameter,
        ))

        files = {
            '/etc/ecs/ecs.config': InitFile(
                content=Sub(textwrap.dedent('''\
                    ECS_CLUSTER=${EcsCluster}
                    # https://github.com/aws/amazon-ecs-agent/issues/1119#issuecomment-357529259
                    ECS_ENABLE_TASK_CPU_MEM_LIMIT=false
                ''')),
                mode="000644",
                owner="root",
                group="root",
            ),
            '/etc/awslogs/awslogs.conf': InitFile(
                content=Sub(textwrap.dedent('''\
                    [general]
                    state_file = /var/lib/awslogs/agent-state

                    [/var/log/docker]
                    file = /var/log/docker
                    log_group_name = /var/log/docker
                    log_stream_name = ${EcsCluster}/{container_instance_id}
                    datetime_format = %Y-%m-%dT%H:%M:%S.%f

                    [/var/log/ecs/ecs-init.log]
                    file = /var/log/ecs/ecs-init.log
                    log_group_name = /var/log/ecs/ecs-init.log
                    log_stream_name = ${EcsCluster}/{container_instance_id}
                    datetime_format = %Y-%m-%dT%H:%M:%SZ

                    [/var/log/ecs/ecs-agent.log]
                    file = /var/log/ecs/ecs-agent.log.*
                    log_group_name = /var/log/ecs/ecs-agent.log
                    log_stream_name = ${EcsCluster}/{container_instance_id}
                    datetime_format = %Y-%m-%dT%H:%M:%SZ

                    [/var/log/cfn-init.log]
                    file = /var/log/cfn-init.log
                    log_group_name = /var/log/cfn-init.log
                    log_stream_name = ${EcsCluster}/{container_instance_id}
                    datetime_format = %Y-%m-%dT%H:%M:%SZ
                ''')),
                mode="000644",
                owner="root",
                group="root",
            ),
            '/etc/awslogs/awscli.conf': InitFile(
                content=Sub(textwrap.dedent('''\
                    [plugins]
                    cwlogs = cwlogs
                    [default]
                    region = ${AWS::Region}
                ''')),
                mode="000600",
                owner="root",
                group="root",
            ),
        }

        init_commands = {
            '00-set-instance-metadata': {
                'command': (
                    'sed -i -e "s/{container_instance_id}/'
                    '$(curl -s http://169.254.169.254/latest/meta-data/instance-id)'
                    '/g" /etc/awslogs/awslogs.conf'
                ),
            },
        }

        services = {
            'sysvinit': InitServices({
                'awslogs': InitService(
                    enabled=True,
                    ensureRunning=True,
                    files=[
                        '/etc/awslogs/awslogs.conf',
                        '/etc/awslogs/awscli.conf',
                    ],
                ),
            }),
        }

        for idx, volume in enumerate(ec2_volumes):
            # Each instance needs a volume
            cmd = f'/root/mount-volume.sh'

            files[cmd] = InitFile(
                content=Sub(render_template(
                    'AutoscalingEbsMount.sh',
                    config=config,
                )),
                mode="000700",
                owner="root",
                group="root",
            )

            init_commands[f'{(idx + 10):02}-mount-volume'] = {
                'command': (
                    f'{cmd} {volume["tag"]} '
                    f'{get_device_mapping(idx + VOLUME_OFFSET)} {volume["mountpoint"]}'
                )
            }

        for idx, cmd in enumerate(commands):
            init_commands[f'{(idx + 20):02}-command'] = {'command': cmd}

        self.launch_config = t.add_resource(LaunchConfiguration(
            'LaunchConfiguration',
            UserData=Base64(Sub(self.userdata)),
            ImageId=Ref(ecs_ami),
            InstanceType=instance_type,
            SecurityGroups=security_groups,
            IamInstanceProfile=Ref(self.instance_profile),
            KeyName=config.ec2_keypair,
            Metadata=Metadata(
                Init({
                    'config': InitConfig(
                        files=InitFiles(files),
                        commands=init_commands,
                        services=services,
                    ),
                }),
            ),
        ))

        self.asg = t.add_resource(AutoScalingGroup(
            'AutoscalingGroup',
            DesiredCapacity=size['default'],
            Tags=[
                AsgTag(key, value, True) for key, value in tags.items()
            ] + [
                AsgTag('Env', config.env, True),
            ],
            LaunchConfigurationName=Ref(self.launch_config),
            MinSize=size['min'],
            MaxSize=size['max'],
            VPCZoneIdentifier=[Ref(i.subnet) for i in subnets],
            HealthCheckType='EC2',
            CreationPolicy=CreationPolicy(
                AutoScalingCreationPolicy=AutoScalingCreationPolicy(
                    MinSuccessfulInstancesPercent='100',
                ),
                ResourceSignal=ResourceSignal(
                    Count='1',
                    Timeout='PT5M',
                ),
            ),
            UpdatePolicy=UpdatePolicy(
                AutoScalingReplacingUpdate=AutoScalingReplacingUpdate(
                    WillReplace=True,
                ),
                AutoScalingRollingUpdate=AutoScalingRollingUpdate(
                    PauseTime='PT5M',
                    MinInstancesInService=size['min'],
                    MaxBatchSize=size.get('batch', 0),
                    WaitOnResourceSignals=True
                )
            )
        ))
