"""Wrapper class for rendering subnets"""
# pylint: disable=too-few-public-methods,too-many-arguments


from troposphere import (
    Ref,
    Tags,
    Template,
)
from troposphere.ec2 import (
    NatGateway,
    NetworkAcl,
    NetworkAclEntry,
    PortRange,
    Route,
    RouteTable,
    Subnet as TroposphereSubnet,
    SubnetNetworkAclAssociation,
    SubnetRouteTableAssociation,
)

from tools import Config


class Subnet:
    """Create a subnet, inbound and outbound NACLs, a route table, and a route
    table association."""

    def __init__(self, t: Template,
                 name: str,
                 cidr: str,
                 vpc: Ref,
                 az: str,
                 gateway: Ref,
                 private: bool = True,
                 nat_eip: str = None,
                ):

        config = Config()

        self.subnet = t.add_resource(TroposphereSubnet(
            name,
            VpcId=vpc,
            CidrBlock=cidr,
            AvailabilityZone=az,
            MapPublicIpOnLaunch=not private,
            Tags=Tags(
                Name=name,
                Env=config.env,
                Visibility='Private' if private else 'Public',
            )
        ))

        self.routetable = t.add_resource(RouteTable(
            f'{name}RouteTable',
            VpcId=vpc,
            Tags=Tags(
                Name=name,
                Env=config.env,
                Visibility='Private' if private else 'Public',
            )
        ))

        self.routetable_assoc = t.add_resource(SubnetRouteTableAssociation(
            f'{name}RouteTableAssociation',
            SubnetId=Ref(self.subnet),
            RouteTableId=Ref(self.routetable)
        ))

        self.nacl = t.add_resource(NetworkAcl(
            f'{name}NetworkAcl',
            VpcId=vpc,
            Tags=Tags(
                Name=name,
                Env=config.env,
                Visibility='Private' if private else 'Public',
            )
        ))

        for flow in ('Inbound', 'Outbound'):
            t.add_resource(NetworkAclEntry(
                f'{name}{flow}NetworkAclEntry',
                NetworkAclId=Ref(self.nacl),
                RuleNumber='100',
                Protocol='6',
                PortRange=PortRange(To='65535', From='0'),
                Egress='true' if flow == 'Outbound' else 'false',
                RuleAction='allow',
                CidrBlock='0.0.0.0/0',
            ))

        self.nacl_assoc = t.add_resource(
            SubnetNetworkAclAssociation(
                f'{name}SubnetNetworkAclAssociation',
                SubnetId=Ref(self.subnet),
                NetworkAclId=Ref(self.nacl),
            )
        )

        if private:
            self.default_route = t.add_resource(Route(
                f'{name}NatRoute',
                RouteTableId=Ref(self.routetable),
                DestinationCidrBlock='0.0.0.0/0',
                NatGatewayId=gateway,
            ))

        else:
            self.default_route = t.add_resource(Route(
                f'{name}DefaultRoute',
                RouteTableId=Ref(self.routetable),
                DestinationCidrBlock='0.0.0.0/0',
                GatewayId=gateway,
            ))

        if nat_eip:
            self.natgw = t.add_resource(NatGateway(
                f'{name}NatGw',
                AllocationId=nat_eip,
                SubnetId=Ref(self.subnet),
            ))
