#!/usr/bin/env python3
"""Transforms for templates"""


import importlib.util
import jinja2
import os
import shutil
from glob import glob

from tools import (
    OUTPUTDIR,
    TEMPLATEDIR,
)


def python(path: str):
    """Render all python files in the path. Not recursive."""
    for f in glob(os.path.join(path, '*.py')):
        dest = prepare(f)
        with open(dest, 'w') as dest_f:
            dest_f.write(import_and_run(f))


def yaml(path: str):
    """Copy all yml files from the path to the destination"""
    for f in glob(os.path.join(path, '*.yml')):
        dest = prepare(f)
        shutil.copyfile(f, dest)


def prepare(path: str) -> str:
    """Make destination directory and return destination filename"""
    destfile = os.path.basename(path).replace('.py', '.yml')
    destdir = os.path.join(OUTPUTDIR, os.path.dirname(path))

    if not os.path.isdir(destdir):
        os.makedirs(destdir)

    return os.path.join(destdir, destfile)

def import_and_run(path: str) -> str:
    """Import and run a template script"""
    spec = importlib.util.spec_from_file_location('script', path)
    script = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(script)

    return script.render()

def render_template(template, **kwargs):
    """Render a template and return a string"""
    t = jinja2.Environment(
        loader=jinja2.FileSystemLoader(TEMPLATEDIR)
    ).get_template(template)

    return t.render(**kwargs)
