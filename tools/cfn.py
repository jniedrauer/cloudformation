"""Cloudformation functions"""


import os
import subprocess
import shutil
import sys
import time
import threading
from glob import glob
from typing import List

from colorama import Fore

import boto3
from botocore.exceptions import ClientError

from tools import (
    OUTPUTDIR,
    Config,
)


DEFAULT_ENTRYPOINT = 'main.yml'
SECRET_IDENTIFIER = 'CfnSecret'

STATUS = dict(
    good=(
        'CREATE_IN_PROGRESS',
        'UPDATE_IN_PROGRESS',
        'UPDATE_COMPLETE_CLEANUP_IN_PROGRESS',
        'REVIEW_IN_PROGRESS'
    ),
    bad=(
        'ROLLBACK_IN_PROGRESS',
        'DELETE_IN_PROGRESS',
        'UPDATE_ROLLBACK_IN_PROGRESS',
        'UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS'
    ),
    failed=(
        'ROLLBACK_COMPLETE',
        'ROLLBACK_FAILED',
        'DELETE_FAILED',
        'DELETE_COMPLETE',
        'UPDATE_ROLLBACK_COMPLETE'
    ),
    finished=(
        'CREATE_COMPLETE',
        'UPDATE_COMPLETE'
    ),
)

SPINCHARS = (
    '⠋',
    '⠙',
    '⠹',
    '⠸',
    '⠼',
    '⠴',
    '⠦',
    '⠧',
    '⠇',
    '⠏',
)

CFN = boto3.client('cloudformation')
S3 = boto3.client('s3')
SSM = boto3.client('ssm')


def validate(stack: str):
    """Validate a given stack"""
    for f in glob(os.path.join(OUTPUTDIR, stack, '*.yml')):
        print('Validating:', os.path.join(stack, os.path.basename(f)))
        try:
            with open(f) as template:
                CFN.validate_template(TemplateBody=template.read())
                print('OK')
        except ClientError as err:
            print(err)
            sys.exit(1)


def diff(stack: str):
    """Print a diff between local and remote copies of a stack"""
    tmpdir = os.path.join(OUTPUTDIR, '.tmp', stack)
    if not os.path.isdir(tmpdir):
        os.makedirs(tmpdir)

    s3_sync(get_s3_path(stack), tmpdir)

    for f in glob(os.path.join(OUTPUTDIR, stack, '*.yml')):
        print('Comparing:', os.path.join(stack, os.path.basename(f)))
        old = os.path.join(OUTPUTDIR, '.tmp', stack, os.path.basename(f))

        if not os.path.isfile(old):
            old = os.devnull

        subprocess.call(['git', '--no-pager', 'diff', '--no-index', old, f])


def sync_to_s3(stack: str):
    """Sync a stack to S3"""
    s3_sync(os.path.join(OUTPUTDIR, stack), get_s3_path(stack))


def deploy(stack: str) -> str:
    """Deploy a stack using AWS CLI and return the stack ID"""
    subprocess.call([
        'aws', 'cloudformation', 'deploy',
        '--template-file', os.path.join(OUTPUTDIR, stack, DEFAULT_ENTRYPOINT),
        '--stack-name', get_stackname(stack),
        '--capabilities', 'CAPABILITY_NAMED_IAM',
        '--s3-bucket', Config().cfn_bucket,
        '--s3-prefix', 'deployed_templates',
        '--parameter-overrides', *get_params(stack),
        '--tags', f'Env={Config().env}',
    ])

    response = CFN.describe_stacks(
        StackName=get_stackname(stack),
    )

    for i in response['Stacks']:
        return i['StackId']


def update_stack(stack: str) -> str:
    """Update a given stack"""
    response = CFN.update_stack(
        StackName=get_stackname(stack),
        TemplateURL=f'{get_s3_path(stack, proto="https")}/{DEFAULT_ENTRYPOINT}',
        Parameters=get_params(stack),
        Capabilities=['CAPABILITY_NAMED_IAM'],
        Tags=[
            {
                'Key': 'env',
                'Value': Config().env,
            }
        ]
    )
    return response['StackId']


def create_stack(stack: str) -> str:
    """Create a new stack"""
    response = CFN.create_stack(
        StackName=get_stackname(stack),
        TemplateURL=f'{get_s3_path(stack, proto="https")}/{DEFAULT_ENTRYPOINT}',
        Parameters=get_params(stack),
        Capabilities=['CAPABILITY_NAMED_IAM'],
        Tags=[
            {
                'Key': 'env',
                'Value': Config().env,
            }
        ]
    )
    return response['StackId']


def poll_for_stack(stack_id) -> bool:
    """Poll for stack until complete or rollback"""
    sys.stdout.write('\n')

    signal = {'msg': '', 'done': False}
    pthread = threading.Thread(target=spinner, args=(signal,))
    pthread.daemon = True
    pthread.start()

    success = None
    while True:
        response = CFN.describe_stacks(StackName=stack_id)
        for stack in response['Stacks']:
            name = stack['StackName']
            status = stack['StackStatus']

            if status in STATUS['good'] + STATUS['finished']:
                color = Fore.GREEN
            if status in STATUS['bad'] + STATUS['failed']:
                color = Fore.RED

            signal['msg'] = f'{name}: {color}{status}{Fore.RESET}'

            if status in STATUS['finished']:
                success = True
            elif status in STATUS['failed']:
                success = False

            if success is not None:
                signal['done'] = True
                pthread.join()
                return success

        time.sleep(5)


def spinner(signal: dict):
    """Use a mutable object as a message to print next to a spinner"""
    while not signal['done']:
        for char in SPINCHARS:
            sys.stdout.write(f'\x1b[2K\r{Fore.BLUE}{char}{Fore.RESET} {signal["msg"]}')
            sys.stdout.flush()
            time.sleep(0.1)

    sys.stdout.write(f'\x1b[2K\r{signal["msg"]}')
    sys.stdout.flush()
    sys.stdout.write('\n')


def package(stack: str, tmpdir: str):
    """Use the AWS CLI to run Cloudformation package"""
    templates = glob(os.path.join(OUTPUTDIR, stack, '*.yml'))

    # Make sure the entrypoint gets transformed last
    entrypoint = templates.index(os.path.join(OUTPUTDIR, stack, DEFAULT_ENTRYPOINT))
    templates.insert(len(templates), templates.pop(entrypoint))

    for f in templates:
        transform_file(f, tmpdir)


def transform_file(tpl: str, tmpdir: str):
    """Package an individual file"""
    tmpf = os.path.join(tmpdir, os.path.basename(tpl))
    shutil.copyfile(tpl, tmpf)

    subprocess.call([
        'aws', 'cloudformation', 'package',
        '--template', tmpf,
        '--s3-bucket', Config().cfn_bucket,
        '--s3-prefix', 'packages',
        '--output-template-file', tpl,
    ])


def get_params(stack: str) -> List[dict]:
    """Generate parameters for the stack"""
    name = get_stackname(stack)

    response = SSM.get_parameters_by_path(
        Path=f'/{Config().env}/{SECRET_IDENTIFIER}/{name}',
        Recursive=False,
        WithDecryption=True,
    )

    params = [
        {
            'ParameterKey': i['Name'].split('/')[-1],
            'ParameterValue': i['Value']
        }
        for i in response['Parameters']
    ]

    params.append({
        'ParameterKey': 'StackName',
        'ParameterValue': name
    })

    return [f'{i["ParameterKey"]}={i["ParameterValue"]}' for i in params]


def stack_exists(stack: str):
    """Check if a cloudformation stack exists"""
    try:
        response = CFN.describe_stacks(StackName=get_stackname(stack))
    except ClientError:
        return False

    return len(response['Stacks']) > 0


def get_s3_path(stack: str, proto: str = 's3') -> str:
    """Given a stack, return its expected path in S3"""
    path = f'{os.path.dirname(stack.strip("/"))}/{get_stackname(stack)}'

    if proto == 's3':
        return f's3://{Config().cfn_bucket}/{path}'
    elif proto == 'https':
        return f'{S3.meta.endpoint_url}/{Config().cfn_bucket}/{path}'

    return ''


def get_stackname(stack: str):
    """Given a stack path, get the expected CFN stack name"""
    name = stack.strip('/').split('/')[-1]
    return f'{name}{Config().env.title()}'


def s3_sync(src: str, dest: str):
    """Sync src to dest. boto3 does not have an S3 sync method"""
    subprocess.call(['aws', 's3', 'sync', src, dest])
