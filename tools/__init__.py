# pylint: disable=wrong-import-position
import os

BASEDIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
CONFIGDIR = os.path.join(BASEDIR, 'config')
OUTPUTDIR = os.path.join(BASEDIR, 'build')
TEMPLATEDIR = os.path.join(BASEDIR, 'tools', 'resources', 'templates')


from .singleton import Singleton
from .config import Config
