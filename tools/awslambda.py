"""Build AWS Lambda packages"""


import os
import shutil
from typing import List

import pip
import requests
import shutil
import yaml


LAMBDA_PATH = 'lambda_modules'
CONFIG_FILE = 'build.yml'


def build(stack: str, tmpdir: str) -> List[str]:
    """Given a stack, compile all AWS lambda projects and return a list
    of zip files"""
    basedir = os.path.join(stack, LAMBDA_PATH)
    config_file = os.path.join(basedir, CONFIG_FILE)
    output = []

    if not os.path.isdir(basedir) or not os.path.isfile(config_file):
        print('No AWS Lambda projects found')
        return output

    with open(config_file) as f:
        config = yaml.safe_load(f)

    builddir = os.path.join(tmpdir, 'build')

    shutil.copytree(basedir, builddir)

    for module in config['modules']:
        output.append(module['name'])

        if module['type'] == 'python':
            build_python(builddir, module['name'])
        elif module['type'] == 'go':
            target_path = os.path.join(
                builddir,
                module['name'] + '.zip',
            )
            download_pkg(target_path, module['url'])
        else:
            raise OSError('Unrecognized Lambda project type')

    return output


def build_python(builddir: str, basename: str):
    """Given a path to a python AWS lambda project and a detination path,
    create a zip archive at the destination path."""
    path = os.path.join(builddir, basename)

    requirements = os.path.join(path, 'requirements.txt')

    if os.path.isfile(requirements):
        pip.main(['install', '-U', '-r', requirements, '-t', path])

    cwd = os.getcwd()
    os.chdir(path)
    try:
        shutil.make_archive(os.path.join(builddir, basename), 'zip', path)
    finally:
        os.chdir(cwd)

def download_pkg(path: str, url: str):
    """Given a url and a path, download package to path"""
    print('Downloading Lambda package from URL:', url, 'to:', path)

    reply = requests.get(url, stream=True, allow_redirects=True)

    if not reply.status_code == requests.codes.ok:
        raise RuntimeError('Failed to download Lambda artifact')

    with open(path, 'wb') as f:
        for chunk in reply.iter_content(1024):
            f.write(chunk)
