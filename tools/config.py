"""Central configuration management using YAML config file"""


import os

import yaml

from tools import (
    CONFIGDIR,
    Singleton,
)


class AttributeDict(dict):
    """Allow access to members as attributes"""

    def __getitem__(self, key: str):
        value = super().__getitem__(key)

        if isinstance(value, dict):
            return AttributeDict(**value)

        return super().__getitem__(key)

    def __getattr__(self, key: str):
        return self.__getitem__(key)

    def __setattr__(self, key: str, value):
        self.__setitem__(key, value)


class Config(AttributeDict, metaclass=Singleton):
    """Maintain a global configuration state"""

    base_config = os.path.join(CONFIGDIR, 'all.yml')
    env_config = os.path.join(CONFIGDIR, '{env}.yml')

    def __init__(self):
        baseconfig = self.get_config(self.base_config)
        super().__init__(**baseconfig)

    def load_env(self, env: str):
        """Load environment configuration"""
        env_file = self.env_config.format(env=env)
        self.update(self.get_config(env_file))

        _newregion = self.region
        _newregion.update(self.regions[self.region.name])
        self['region'] = _newregion

    @staticmethod
    def get_config(path: str) -> dict:
        """Return a dict from a config file"""
        with open(path) as f:
            return yaml.safe_load(f)
