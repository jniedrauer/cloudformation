"""Interactions with the github API"""


from typing import List

import requests


META_URL = 'https://api.github.com/meta'


def get_webhook_ips() -> List[str]:
    """Get a list of IP addresses used by github webooks"""
    reply = requests.get(META_URL)

    return reply.json()['hooks']
