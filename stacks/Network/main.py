"""Base network configuration for the {env} environment"""


from troposphere import (
    GetAtt,
    Join,
    Parameter,
    Ref,
    Tags,
    Template,
)
from troposphere.ec2 import (
    InternetGateway,
    VPC,
    VPCGatewayAttachment,
)
from troposphere.ec2 import (
    SecurityGroup,
    SecurityGroupRule,
    SecurityGroupIngress,
)
from troposphere.ssm import Parameter as SsmParameter
from troposphere.cloudformation import Stack

from tools import Config
from tools.resources import Subnet


def render() -> str:
    """Default entrypoint"""
    config = Config()

    t = Template()

    t.add_version('2010-09-09')

    t.add_description(__doc__.format(env=config.env))

    t.add_parameter(Parameter(
        'StackName',
        Description='Cloudformation stack name',
        Type='String',
    ))

    vpc_basename = f'PrimaryVpc{config.env.title()}'

    vpc = t.add_resource(VPC(
        vpc_basename,
        EnableDnsSupport='true',
        CidrBlock=config.vpcs.primary.cidr,
        EnableDnsHostnames='true',
        Tags=Tags(
            Name=vpc_basename,
        )
    ))

    t.add_resource(SsmParameter(
        f'{vpc_basename}VpcParameter',
        Name=f'/{config.env}/PrimaryVpc/VpcId',
        Type='String',
        Value=Ref(vpc)
    ))

    igw = t.add_resource(InternetGateway(
        f'{vpc_basename}InternetGateway',
    ))

    t.add_resource(SsmParameter(
        f'{vpc_basename}IgwParameter',
        Name=f'/{config.env}/PrimaryVpc/Igw',
        Type='String',
        Value=Ref(igw)
    ))

    t.add_resource(VPCGatewayAttachment(
        f'{vpc_basename}IgwAttachment',
        VpcId=Ref(vpc),
        InternetGatewayId=Ref(igw),
    ))

    public_subnets = []

    for idx, params in enumerate(config.vpcs.primary.subnets.public.networking):
        az = config.region.name + config.region.AZs[params['az']]
        subnet_basename = f'PrimaryNetworkingSubnet{idx:02}{config.env.title()}'

        subnet = Subnet(
            name=subnet_basename,
            cidr=params['cidr'],
            vpc=Ref(vpc),
            az=az,
            gateway=Ref(igw),
            private=False,
            nat_eip=params.get('nat_eip'),
            t=t,
        )

        public_subnets.append(subnet)

    common_sg = t.add_resource(SecurityGroup(
        'VpnSecurityGroup',
        GroupDescription=f'{config.env.title()} common security group',
        VpcId=Ref(vpc),
        SecurityGroupEgress=[
            SecurityGroupRule(
                CidrIp='0.0.0.0/0',
                FromPort='-1',
                IpProtocol='-1',
                ToPort='-1',
            )
        ],
        SecurityGroupIngress=[
            SecurityGroupRule(
                CidrIp=i,
                FromPort='22',
                IpProtocol='tcp',
                ToPort='22',
            )
            for i in config.access_whitelist
        ]
    ))

    t.add_resource(SsmParameter(
        f'CommonSgParam',
        Name=f'/{config.env}/PrimaryVpc/CommonSg',
        Type='String',
        Value=GetAtt(common_sg, 'GroupId')
    ))

    return t.to_yaml(long_form=True)
