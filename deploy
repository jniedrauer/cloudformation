#!/usr/bin/env python3
"""Deploy Cloudformation stacks using troposphere or yaml templates"""


import argparse
import os
import shutil
import sys
import tempfile

from tools import (
    Config,
    awslambda,
    render,
)


def main():
    """Entrypoint"""
    arg_parser = build_arg_parser()
    args = arg_parser.parse_args()

    Config().load_env(args.environment)

    os.environ['AWS_PROFILE'] = Config().aws_profile

    # Import this after AWS_PROFILE has been set
    from tools import cfn

    if not os.path.isdir(args.stack):
        error(f'Stack does not exist: {args.stack}')

    render.python(args.stack)
    render.yaml(args.stack)

    tmpdir = tempfile.mkdtemp()
    try:
        awslambda.build(args.stack, tmpdir)
        cfn.package(args.stack, tmpdir)
    finally:
        shutil.rmtree(tmpdir)

    cfn.validate(args.stack)
    cfn.diff(args.stack)

    prompt_for_action(
        'Continue deployment?',
        action={'func': lambda: None, 'args': []},
        noaction={'func': sys.exit, 'args': []},
        default=False,
    )

    cfn.sync_to_s3(args.stack)

    stack_id = cfn.deploy(args.stack)

    if not cfn.poll_for_stack(stack_id):
        sys.exit(1)


def prompt_for_action(msg: str, action: dict = None, noaction: dict = None, default: bool = False):
    """Prompt the user for yes or no and perform one of two actions"""
    default_str: str = '[Y/n]' if default else '[y/N]'

    answer: str = input(f'{msg} {default_str}: ')

    if answer.upper().startswith('Y') or (not answer and default):
        action['func'](*action['args'])
    else:
        noaction['func'](*noaction['args'])


def build_arg_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument('--env', action='store', default='dev',
                        choices=Config().environments,
                        dest='environment',
                        help='target environment for deployment')
    parser.add_argument('stack', metavar='STACK',
                        help='Cloudformation stack to deploy')

    return parser


def error(msg):
    """Print message and quit"""
    print(msg)
    sys.exit(1)


if __name__ == '__main__':
    main()
